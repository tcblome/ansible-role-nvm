#!/usr/bin/env bash

INSTALL_VER="$1"
INSTALL_DIR="$2"

NVM_SOURCE=https://github.com/creationix/nvm.git

if [ -d "$INSTALL_DIR/.git" ]; then
  echo "=> nvm is already installed in $INSTALL_DIR, trying to update using git"
  command printf '\r=> '
  command git --git-dir="$INSTALL_DIR"/.git --work-tree="$INSTALL_DIR" fetch origin tag "${INSTALL_VER}" --depth=1 2> /dev/null || {
    echo >&2 "Failed to update nvm, run 'git fetch' in $INSTALL_DIR yourself."
    exit 1
  }
else
  # Cloning to $INSTALL_DIR
  echo "=> Downloading nvm from git to '$INSTALL_DIR'"
  command printf '\r=> '
  mkdir -p "${INSTALL_DIR}"
  if [ "$(ls -A "${INSTALL_DIR}")" ]; then
    command git init "${INSTALL_DIR}" || {
      echo >&2 'Failed to initialize nvm repo. Please report this!'
      exit 2
    }
    command git --git-dir="${INSTALL_DIR}/.git" remote add origin "${NVM_SOURCE}" 2> /dev/null \
      || command git --git-dir="${INSTALL_DIR}/.git" remote set-url origin "${NVM_SOURCE}" || {
      echo >&2 'Failed to add remote "origin" (or set the URL). Please report this!'
      exit 2
    }
    command git --git-dir="${INSTALL_DIR}/.git" fetch origin tag "${INSTALL_VER}" --depth=1 || {
      echo >&2 'Failed to fetch origin with tags. Please report this!'
      exit 2
    }
  else
    command git -c advice.detachedHead=false clone "${NVM_SOURCE}" -b "${INSTALL_VER}" --depth=1 "${INSTALL_DIR}" || {
      echo >&2 'Failed to clone nvm repo. Please report this!'
      exit 2
    }
  fi
fi
command git -c advice.detachedHead=false --git-dir="$INSTALL_DIR"/.git --work-tree="$INSTALL_DIR" checkout -f --quiet "${INSTALL_VER}"
if [ ! -z "$(command git --git-dir="$INSTALL_DIR"/.git --work-tree="$INSTALL_DIR" show-ref refs/heads/master)" ]; then
  if command git --git-dir="$INSTALL_DIR"/.git --work-tree="$INSTALL_DIR" branch --quiet 2>/dev/null; then
    command git --git-dir="$INSTALL_DIR"/.git --work-tree="$INSTALL_DIR" branch --quiet -D master >/dev/null 2>&1
  else
    echo >&2 "Your version of git is out of date. Please update it!"
    command git --git-dir="$INSTALL_DIR"/.git --work-tree="$INSTALL_DIR" branch -D master >/dev/null 2>&1
  fi
fi

echo "=> Compressing and cleaning up git repository"
if ! command git --git-dir="$INSTALL_DIR"/.git --work-tree="$INSTALL_DIR" reflog expire --expire=now --all; then
  echo >&2 "Your version of git is out of date. Please update it!"
fi
if ! command git --git-dir="$INSTALL_DIR"/.git --work-tree="$INSTALL_DIR" gc --auto --aggressive --prune=now ; then
  echo >&2 "Your version of git is out of date. Please update it!"
fi
