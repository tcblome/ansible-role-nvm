Ansible role: nvm
=========

![pipeline](https://gitlab.com/tcblome/ansible-role-nvm/badges/master/build.svg)

This role installs [nvm](https://nvm.sh/).


Requirements
------------
git

Installation
------------

Using `ansible-galaxy`:

```shell
$ ansible-galaxy install https://gitlab.com/tcblome/ansible-role-nvm.git
```

Using `requirements.yml`:
```yaml
 src: git+https://gitlab.com/tcblome/ansible-role-nvm.git
 name: tcblome.nvm
```
Using `git`:
```shell
$ git clone https://gitlab.com/tcblome/ansible-role-nvm.git tcblome.nvm
```

Role Variables
--------------
```yaml

# defaults file for ansible-role-nvm
#
# nvm_version	*	# version of nvm to install

```

Dependencies
------------

This role can be used independently.

Example Playbook
----------------

Sample :
```
    - hosts: servers
      roles:
        - { role: tcblome.nvm,
	         nvm_version: "0.33.11"
	        }
```

License
------------------
[LICENSE](LICENSE)

Author Information
------------------

This role is published for private use by @tcblome
